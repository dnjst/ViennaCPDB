# ViennaCPDB

## Name
Vienna CellPhoneDB.

## Description
A fork of the CellPhoneDB database providing simple access to entries by gene name and Ensembl ID, as well as useful metadata annotations.

## Usage
The included database can be fed into interaction programs using simply the "ligand" and "receptor" columns for (human) gene names, "ligand_ensembl" and "receptor_ensembl" columns for (human) Ensembl stable gene IDs, or "ligand_uniprot" and "receptor_uniprot" for (human) UniProt IDs.

Useful annotations include:

- "classification": Signaling pathways as defined by CellPhoneDB
- "directionality": Ligand-Receptor, Ligand-Ligand, Receptor-Receptor, Adhesion-Adhesion, etc.
- "modulatory_effect": "Inhibitory" for decoy receptors and secreted inhibitors
- "annotation": Classification of interactions into "Secreted Signaling", "Small Molecule-Mediated", "Cell-Cell Contact", and "ECM-Receptor" categories, for differential handling of different biological types. Expanded from the CellChatDB database.
- "growth_factor": Whether the interaction in question is documented to regulate target cell proliferation or chemotaxis, such as by belonging to the gene ontology category "growth factor" (GO:0008083).
- "green_beard_1MB": Whether the ligand and receptor locus (in the human) are either homotypic (the same protein), or located on the same chromosome within 1 Megabase. Based upon the [green beard prediction](https://doi.org/10.1073/pnas.93.13.6547).
- "connectome_mode": Annotation of interactions based upon the Connectome database. Currently incomplete and not recommended; use CellPhoneDB "classification" instead.
- "pathway_name": Annotation of interactions based upon the CellChatDB database. Currently incomplete and not recommended; use CellPhoneDB "classification" instead.

N.B.: The following two interactions appear twice in the database due to mediation by different splice variants, and should in most cases be deduplicated before usage:
`ALOX5_ALOX5AP_LTC4S -> CYSLTR1`
`IFNL3 -> IFNLR1_IL10RB`

## Minimal Usage

```
## Python loading of latest ViennaCPDB for Liana
import pandas as pd
intercell_merged = pd.read_csv("https://gitlab.com/dnjst/ViennaCPDB/-/raw/main/ViennaCPDB.csv")
intercell_liana = intercell_merged.loc[:,["ligand","receptor"]].drop_duplicates()

# code calling liana.mt.rank_aggregate() with resource = intercell_liana as an argument...

```

## Contributing
Contributions are welcome on the "issues" tab. Contributions related to metadata tagging, or organization, are most useful in this repository. New interactions are also welcome, but should also be submitted [upstream to CellPhoneDB curators](https://github.com/ventolab/cellphonedb-data).

## Authors, acknowledgment, and licensing
The enclosed database is extended and modified from CellPhoneDB, available at https://github.com/ventolab/cellphonedb-data.
The CellPhoneDB database is to my knowledge unlicenced, but the CellPhoneDB tool is protected under the MIT License.
Additional curation was performed by Daniel Stadtmauer.
Select annotations from CellChatDB were imported, which is licensed under a GPL v3 license https://github.com/sqjin/CellChat.
Therefore, this compilation should be treated as GPL v3.

## Alternatives
For a version including additional [Liana consensus](https://github.com/saezlab/liana) interactions and CellPhoneDB v4.1.0, see [ViennaCCCdb](https://github.com/JanLeipzig/ViennaCCCdb).

## Project status
Under active development.

## Version
`ViennaCPDB Version: 20240314`
`CellPhoneDB Version: 5.1.0 pre-release`